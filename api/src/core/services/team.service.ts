import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User, Team, Members } from '../../data/entities';
import { Repository } from 'typeorm';
import { CreateTeamDTO } from '../../models';

import { UsersService } from './users.service';

@Injectable()
export class TeamService {

    public constructor(
        @InjectRepository(Team) private readonly teamRepository: Repository<Team>,
        @InjectRepository(Members) private readonly membersRepository: Repository<Members>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        private readonly userService: UsersService,
    ) { }

    async createNewTeam(user: User, team: CreateTeamDTO): Promise<Team> {
        const teamAuthor = await this.userRepository.findOne({ id: user.id });
        const newTeam = new Team();

        newTeam.author = Promise.resolve(teamAuthor);
        newTeam.name = team.name;
        newTeam.members = Promise.resolve([teamAuthor]);
        const savedTeam = await this.teamRepository.save(newTeam);

        const newTeamMembers = new Members();
        newTeamMembers.member = Promise.resolve(teamAuthor);
        newTeamMembers.team = Promise.resolve(savedTeam);
        newTeamMembers.isInTeam = true;

        await this.membersRepository.save(newTeamMembers);
        return savedTeam;
    }

 

    async addNewTeamMember(user: User, teamId: string, userId: string): Promise<any> {
        const foundTeam = await this.teamRepository.findOne({ id: teamId });
        const foundUser = await this.userRepository.findOne({ id: userId });

        const userExistsInMembersTable = await this.membersRepository.findOne({
            relations: ['member'],
            where: { member: foundUser },
        });

        if (!userExistsInMembersTable) {
            const teamMemberToAdd = new Members();
            teamMemberToAdd.isInTeam = true;
            teamMemberToAdd.member = Promise.resolve(foundUser);
            teamMemberToAdd.team = Promise.resolve(foundTeam);

            return await this.membersRepository.save(teamMemberToAdd);
        }

        if (userExistsInMembersTable.isInTeam === false) {
            userExistsInMembersTable.isInTeam = !userExistsInMembersTable.isInTeam;
            return await this.membersRepository.save(userExistsInMembersTable);
        }

        throw new BadRequestException(`User with name ${foundUser.name} is already part of your team!`);
    }


    async getAllTeams(): Promise<Team[]> {
        return await this.teamRepository.find();
    }

    async getAllTeamWorkItems() {
        const foundTeamWorkItems = await this.teamRepository.find({
            relations: ['workItems'],
        });

        return foundTeamWorkItems;
    }

    async getTeamMembers(teamId: string): Promise<User[]> {
        const foundTeam = await this.teamRepository.findOne({ id: teamId });

        const foundTeamMembers = await this.membersRepository.find({
            relations: ['member'],
            where: {
                team: foundTeam,
            },
        });

        const filteredMembers: any = foundTeamMembers.filter(x => x.isInTeam === true);

        return filteredMembers.map(x => x.__member__);
    }

    async getTeamDetails(teamId: string): Promise<any> {
        const team = await this.teamRepository.findOne({ id: teamId });
        const teamMembers = await this.getTeamMembers(teamId);

        return {
            team,
            teamMembers,
        };
    }
}

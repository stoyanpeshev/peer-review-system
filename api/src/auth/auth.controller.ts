import { Controller, Post, Body, ValidationPipe, UseGuards, Delete } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginDTO, RegisterUserDTO } from '../models/index';
import { FindUserGuard, IsUserRegistered } from '../common/guards/index';

@Controller('auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post('registration')
    @UseGuards(IsUserRegistered)
    async register(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: RegisterUserDTO): Promise<object> {
        return await this.authService.register(user);
    }

    @Post('session')
    @UseGuards()
    async login(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserLoginDTO): Promise<object | boolean> {
        return await this.authService.login(user);
    }

    @Delete('session')
    @UseGuards()
    public async logoutUser() {
        return { message: 'Successful logout !' };
    }
}

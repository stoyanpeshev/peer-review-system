import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/index';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { JwtService } from '@nestjs/jwt';
import { UserRole } from '../common/enums/user-role.enum';
import { Role } from '../data/entities/role';
import { UserLoginDTO, ReturnUserDTO, RegisterUserDTO } from '../models/index';
import { Inbox } from '../data/entities/inbox';
import { MailerService } from '@nest-modules/mailer';

@Injectable()
export class AuthService {

    constructor(private readonly mailerService: MailerService,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
        private readonly jwtService: JwtService,
    ) { }

    async checkIfUserExists(payload: any) {
        return await this.userRepository.findOne({
            where: {
                email: payload.email,
            },
        });
    }

    async register(user: RegisterUserDTO): Promise<object> {
        const membersRole = await this.rolesRepository.findOne({
            where: {
                name: UserRole.Basic,
            },
        });
        const userSave = await {
            ...user, password: await bcrypt.hash(user.password, 10), roles: [membersRole],
        };
        const saveOneUser = await this.userRepository.save(userSave);

        this
        .mailerService
        .sendMail({
          to: `${user.email}`,
          from: 'codenetix.telerik@gmail.com',
          subject: 'CODENETIX - Successful registration.',
          text: 'Successful registration.',
          html: `<h4>Hello ${user.name}, your registration was successful.<br><br>For more info visit: www.codenetix.com<br><br>Best regards</h4>`,
        })
        .then(() => {})
        .catch(() => {});

        return {
            message: `Successfull registration, you are automatically logged in!`,
            user: plainToClass(ReturnUserDTO, saveOneUser, {
                excludeExtraneousValues: true,
            }),
        };
    }

    async login(user: UserLoginDTO): Promise<object | boolean> {
        const userFound = await this.userRepository.findOne({ where: { email: user.email } });
        if (!userFound) {
            throw new BadRequestException(`User with email '${user.email}' does not exist!`);
        }

        const result = await bcrypt.compare(user.password, userFound.password);
        if (!result) {
            throw new BadRequestException(`Wrong credentials, try again!`);
        }

        const userRoles = await userFound.roles;
        const displayUserRoles = userRoles.map((role) => role.name);
        const token = await this.jwtService.sign({ ...user });

        return {
            message: `You've logged in successfully!`,
            user: plainToClass(ReturnUserDTO, userFound, {
                excludeExtraneousValues: true,
            }),
            role: displayUserRoles,
            token,
        };
    }
}

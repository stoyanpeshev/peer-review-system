
import "reflect-metadata";
import { createConnection, Repository } from "typeorm";
import { Members, Team, User } from "./data/entities";
import { Inbox } from "./data/entities/inbox";
import { connect } from "net";
import { Reviewers } from "./data/entities/reviewers";
import { Notification } from "./data/entities/notification";
const main = async () => {
    const connection = await createConnection();

    const membersRepo: Repository<Members> = connection.manager.getRepository(Members);
    const teamRepo: Repository<Team> = connection.manager.getRepository(Team);
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const reviewersRepo: Repository<Reviewers> = connection.manager.getRepository(Reviewers);
    const notificationRepo: Repository<Notification> = connection.manager.getRepository(Notification);

};

main().catch(console.error);

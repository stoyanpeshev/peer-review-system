<div align="center">
![alt text](https://symphony.com/images/partners/tick42.png "Alex Tagarev's Final Telerik Project")
</div>
# Tick 42 - Peer Review System

Following these instructions you will be able to run this project on your local machine.

## Prerequisites

The technologies below must be installed in order to run this application:

#### Front End:

* Typescript
* Angular 7+
* Angular CLI

#### Back End:

* MySQL/MariaDB client
* NodeJS
* NPM
* NestJS
* Type ORM

## Installation

To run the project:

#### Back End:

* Create new DB schema - testdb - root // root

* Navigate to ../peer-review-system/api

* Using your favorite console write these commands:

npm install

npm run typeorm -- migration:generate -n initial

npm run typeorm -- migration:run

npm run start:dev

#### Front End:

* Navigate to ../peer-review-system/client

In console write these commands:

npm install

ng s -o

## Author

| #   | First Name | Last Name |
| :-: |:----------:| :--------:|
| 1.  | Alex       | Tagarev   |

## License
[MIT](https://choosealicense.com/licenses/mit/)

import { UserLogin } from './../../common/interfaces/user-login';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UserRegister } from './../../common/interfaces/user-register';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    });
  }

  public onRegister(): void {
    const newUser: UserRegister = this.registerForm.value;
    const loginUser: UserLogin = {
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
    };

    this.authService.register(newUser).subscribe(
      () => {
        // this.notificator.success('Registration Successful !');
        this.authService.login(loginUser).subscribe(
          (data) => {
            this.notificator.success(`Hello, ${data.user.name} !`);
            this.notificator.success(`Create your first team !`);
            this.router.navigate(['/teams/create']);
          },
          () => {
            this.notificator.error('Login Failed !');
          }
        );
        this.router.navigate(['/home']);
      },
      () => {
        this.notificator.error('Registration Failed !');
      }
    );
  }

}

export interface CreateComment {
  title: string;
  description: string;
}

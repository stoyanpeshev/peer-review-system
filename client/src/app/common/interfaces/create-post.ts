export interface CreatePost {
  	title: string;
	description: string;
	comments: string;
	reviewers: [];
  	teamId: string;
}

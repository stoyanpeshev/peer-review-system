import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommentInterface } from '../../common/interfaces/show-comment';

@Component({
  selector: 'app-post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.css']
})
export class PostCommentsComponent implements OnInit {
  @Input() public comment: CommentInterface;
  @Input() public loggedInUser: string;

  @Output() public liked = new EventEmitter();
  @Output() public disliked = new EventEmitter();
  @Output() public updatedItem = new EventEmitter<CommentInterface>();
  @Output() public deletedItem = new EventEmitter<string>();

  constructor(
  ) { }

  ngOnInit() {
  }

  public itemLike(): void {
    this.liked.emit(this.comment.id);
  }

  public itemDislike(): void {
    this.disliked.emit(this.comment.id);
  }

  public updateItem(comment: CommentInterface): void {
    this.comment = comment;
    this.updatedItem.emit(this.comment);
  }

  public deleteItem(): void {
    this.deletedItem.emit(this.comment.id);
  }
}

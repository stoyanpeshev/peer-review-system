import { PostsDataService } from './posts-data.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Post } from '../../../app/common/interfaces/post';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostDetailsResolverService implements Resolve<Post | {post: Post}> {

  constructor(
    private readonly postsDataService: PostsDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const postID = route.params['id'];
    return this.postsDataService.singlePost(postID)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({post: null});
        }
      ));
  }
}

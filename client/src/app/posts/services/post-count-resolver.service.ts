import { PostsDataService } from './posts-data.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Post } from '../../../app/common/interfaces/post';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostCountResolverService implements Resolve<number | {count: number}> {

  constructor(
    private readonly postsDataService: PostsDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.postsDataService.allPostsCount()
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({count: null});
        }
      ));
  }
}

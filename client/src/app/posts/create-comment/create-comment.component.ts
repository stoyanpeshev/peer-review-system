import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {
  public commentForm: FormGroup;
  public currentContent: string;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    placeholder: 'Comment',
    translate: 'no',
    sanitize: false,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
  };


  @Output() public createComment = new EventEmitter<string>();

  constructor(
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      title: ['', [Validators.minLength(3), Validators.required]],
      description: ['', [Validators.minLength(3), Validators.required]],
    });
  }

  public createCommentOfPost(): void {
    this.createComment.emit(this.commentForm.value.comment);
    this.commentForm.reset();
  }
}

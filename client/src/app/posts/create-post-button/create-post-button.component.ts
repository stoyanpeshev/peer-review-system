import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post-button',
  templateUrl: './create-post-button.component.html',
  styleUrls: ['./create-post-button.component.css']
})
export class CreatePostButtonComponent implements OnInit {

  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  public createReviewReq(){
    const teamId = window.location.href.split('/')[4];
    this.router.navigate([`posts/create/${teamId}`]);
  }

}

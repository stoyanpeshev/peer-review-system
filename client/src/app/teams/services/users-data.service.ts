import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Post } from '../../common/interfaces/post';
import { CommentInterface } from '../../common/interfaces/show-comment';
import { CreateTeam } from 'src/app/common/interfaces/create-team';
import { Team } from 'src/app/common/interfaces/team';
import { TeamMembers } from 'src/app/common/interfaces/team-members';

@Injectable({
    providedIn: 'root'
})
export class TeamsDataService {
    constructor(
        private readonly http: HttpClient,
    ) { }

    public createPost(team: CreateTeam): Observable<Team> {
        return this.http.post<Team>('http://localhost:3000/teams', team);
      }

    public allUsers(): Observable<User[]> {
        return this.http.get<User[]>('http://localhost:3000/users/teams');
    }

    public singleUser(userId: string): Observable<Team> {
        return this.http.get<Team>(`http://localhost:3000/teams/${userId}`);
    }

    public postsOfUser(userId: string): Observable<TeamMembers[]> {
        return this.http.get<TeamMembers[]>(`http://localhost:3000/teams/members/${userId}`);
    }

    public commentsOfUser(userId: string): Observable<CommentInterface[]> {
        return this.http.get<CommentInterface[]>(`http://localhost:3000/teams/workItems/`);
    }

    public userFriends(userId: string): Observable<User[]> {
        return this.http.get<User[]>(`http://localhost:3000/api/users/${userId}/friends`);
    }

    public addFriend(targetUserId: string): Observable<User> {
        return this.http.post<User>(`http://localhost:3000/api/users/${targetUserId}/friends`,{});
    }

    public removeFriend(targetUserId: string): Observable<User> {
        return this.http.delete<User>(`http://localhost:3000/api/users/${targetUserId}/friends`,{});
    }
}


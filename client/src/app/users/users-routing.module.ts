import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserResolverService } from './services/user-resolver.service';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserProfileResolverService } from './services/user-proflie-resolver.service';

const routes: Routes = [
    {
      path: '', component: UsersComponent, 
      resolve: {users: UserResolverService} 
    },
    {
      path: ':id', component: UserProfileComponent, 
      resolve: { user: UserProfileResolverService },
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {

}

import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Post } from '../../common/interfaces/post';
import { CommentInterface } from '../../common/interfaces/show-comment';

@Injectable({
    providedIn: 'root'
})
export class UsersDataService {
    constructor(
        private readonly http: HttpClient,
    ) { }

    public allUsers(): Observable<User[]> {
        return this.http.get<User[]>('http://localhost:3000/users');
    }

    public getTeamMembers(teamId: string): Observable<any[]> {
        return this.http.get<any[]>(`http://localhost:3000/teams/members/${teamId}`);
    }

    public singleUser(userId: string): Observable<User> {
        return this.http.get<User>(`http://localhost:3000/users/${userId}`);
    }

    public postsOfUser(userId: string): Observable<Post[]> {
        return this.http.get<Post[]>(`http://localhost:3000/users/reviews/`);
    }

    public commentsOfUser(userId: string): Observable<CommentInterface[]> {
        return this.http.get<CommentInterface[]>(`http://localhost:3000/api/posts/comments/${userId}`);
    }

    public userFriends(userId: string): Observable<User[]> {
        return this.http.get<User[]>(`http://localhost:3000/api/users/${userId}/friends`);
    }

    public addFriend(teamId: string, userId: string): Observable<any> {
        return this.http.put<any>(`http://localhost:3000/teams/${teamId}/${userId}`,{});
    }

    public removeFriend(targetUserId: string): Observable<User> {
        return this.http.delete<User>(`http://localhost:3000/api/users/${targetUserId}/friends`,{});
    }
}

